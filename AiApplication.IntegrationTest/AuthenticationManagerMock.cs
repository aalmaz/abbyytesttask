﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

namespace AiApplication.IntegrationTest
{
    internal class AuthenticationManagerMock : IAuthenticationManager
    {
        public IEnumerable<AuthenticationDescription> GetAuthenticationTypes()
        {
            return new List<AuthenticationDescription>();
        }

        public IEnumerable<AuthenticationDescription> GetAuthenticationTypes(Func<AuthenticationDescription, bool> predicate)
        {
            return new List<AuthenticationDescription>();
        }

        public Task<AuthenticateResult> AuthenticateAsync(string authenticationType)
        {
            return new Task<AuthenticateResult>(() => new AuthenticateResult(new ClaimsIdentity(), new AuthenticationProperties(), new AuthenticationDescription()));
        }

        public Task<IEnumerable<AuthenticateResult>> AuthenticateAsync(string[] authenticationTypes)
        {
            return new Task<IEnumerable<AuthenticateResult>>(() => new List<AuthenticateResult>());
        }

        public void Challenge(AuthenticationProperties properties, params string[] authenticationTypes)
        {
        }

        public void Challenge(params string[] authenticationTypes)
        {
        }

        public void SignOut(AuthenticationProperties properties, params string[] authenticationTypes)
        {

        }

        public void SignOut(params string[] authenticationTypes)
        {

        }

        public ClaimsPrincipal User { get; set; }
        public AuthenticationResponseChallenge AuthenticationResponseChallenge { get; set; }
        public AuthenticationResponseGrant AuthenticationResponseGrant { get; set; }
        public AuthenticationResponseRevoke AuthenticationResponseRevoke { get; set; }

        public void SignIn(params ClaimsIdentity[] identities)
        {

        }

        public void SignIn(AuthenticationProperties properties, params ClaimsIdentity[] identities)
        {

        }
    }
}