﻿using AiApplication.Models;
using AiApplication.Services;
using MongoDB.Driver;
using NUnit.Framework;

namespace AiApplication.IntegrationTest
{
    public class IntegrationBase 
    {
        protected IMongoDatabase Database;
        protected IMongoCollection<ApplicationUser> Users;
        protected AccountService AccountService
        {
            get
            {
                var userManager = new ApplicationUserManager(Users);
                var applicationSignInManager = new ApplicationSignInManager(userManager, new AuthenticationManagerMock());
                return new AccountService(applicationSignInManager, userManager);
            }
        }

        [SetUp]
        public void BeforeEachTest()
        {
            var client = new MongoClient(ConfigProvider.TestConnectionString);
            Database = client.GetDatabase(ConfigProvider.TestDbName);
            Users = Database.GetCollection<ApplicationUser>("users");
            Users.DeleteMany(x => true);
        }
    }
}