﻿using System.Linq;
using AiApplication.Models;
using MongoDB.Driver;
using NUnit.Framework;

namespace AiApplication.IntegrationTest
{
    public class IntegrationUsers: IntegrationBase
    {
        [Test]
        public void EnsureThatUserAdded()
        {
            var newLogin = new LoginInfoModel
            {
                Email = "test@test.me",
                Name = "test",
                SocialNetwork = "testSocialNetwork"
            };

            Users.DeleteMany(x => x.Email == newLogin.Email);

            var prevCount = Users.AsQueryable().Count();

            AccountService.LogIn(newLogin.ToExternalLoginInfo());

            Assert.That(Users.Count(x => true), Is.EqualTo(prevCount + 1));
        }

        [Test]
        public void EnsureThatUserSocialNetworkAppended()
        {
            var newLogin = new LoginInfoModel
            {
                Email = "test@test.me",
                Name = "test",
                SocialNetwork = "testSocialNetwork"
            };
            
            AccountService.LogIn(newLogin.ToExternalLoginInfo());

            var user = Users.Find(x => x.Email == newLogin.Email).First();

            Assert.That(user.Logins.Count, Is.EqualTo(1));

            newLogin.SocialNetwork = "newTestSocialNetwork";

            AccountService.LogIn(newLogin.ToExternalLoginInfo());

            user = Users.Find(x => x.Email == newLogin.Email).First();

            Assert.That(user.Logins.Count, Is.EqualTo(2));
            Assert.That(user.Logins.Any(x => x.LoginProvider == "testSocialNetwork"), Is.True);
            Assert.That(user.Logins.Any(x => x.LoginProvider == "newTestSocialNetwork"), Is.True);
        }

        [Test]
        public void EnsureThatAllExistingUsersAreListed()
        {
            var newLogin = new LoginInfoModel
            {
                Email = "test@test.me",
                Name = "test",
                SocialNetwork = "testSocialNetwork"
            };

            var newLogin2 = new LoginInfoModel
            {
                Email = "test2@test.me",
                Name = "test2",
                SocialNetwork = "testSocialNetwork"
            };

            AccountService.LogIn(newLogin.ToExternalLoginInfo());
            AccountService.LogIn(newLogin2.ToExternalLoginInfo());

            var usersCount = Users.AsQueryable().Count();

            var users = AccountService.ListUsers();

            Assert.That(usersCount, Is.EqualTo(2));
            Assert.That(users.Count, Is.EqualTo(usersCount));
        }
    }
}