﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net.Http;
using System.Web.Http;
using AiApplication.Models;
using AiApplication.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace AiApplication.Controllers.Api
{
    public class DataController : ApiController
    {
        public ApplicationSignInManager SignInManager => Request.GetOwinContext().Get<ApplicationSignInManager>();
        public ApplicationUserManager UserManager => Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

        public IdentityResult Auth(LoginInfoModel loginInfoModel)
        {
            Contract.Requires(loginInfoModel != null);
            return new AccountService(SignInManager, UserManager).LogIn(loginInfoModel.ToExternalLoginInfo());
        }

        public IList<UserModel> List()
        {
            return new AccountService(SignInManager, UserManager).ListUsers();
        }
    }
}
