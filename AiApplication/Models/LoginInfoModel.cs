﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace AiApplication.Models
{
    public class LoginInfoModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string SocialNetwork { get; set; }

        public ExternalLoginInfo ToExternalLoginInfo()
        {
            var claimsIdentity = new ClaimsIdentity(new List<Claim> { new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", Name) });

            return new ExternalLoginInfo
            {
                Login = new UserLoginInfo(SocialNetwork, Guid.NewGuid().ToString()),
                Email = Email,
                ExternalIdentity = claimsIdentity
            };

        }
    }
}