﻿namespace AiApplication.Models
{
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class UserModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string SocialNetworks { get; set; }
    }
}
