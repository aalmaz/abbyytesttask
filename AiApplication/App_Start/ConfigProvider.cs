﻿using System.Collections.Specialized;

namespace AiApplication
{
    public class ConfigProvider
    {
        private static NameValueCollection _settings;

        private static NameValueCollection Settings => _settings ?? (_settings = System.Configuration.ConfigurationManager.AppSettings);

        public static string LinkedInClientId => Settings["LinkedInClientId"];

        public static string LinkedInClientSecret => Settings["LinkedInClientSecret"];

        public static string FacebookClientId => Settings["FacebookClientId"];

        public static string FacebookClientSecret => Settings["FacebookClientSecret"];

        public static string MainDbName => Settings["MainDbName"];

        public static string TestDbName => Settings["TestDbName"];

        public static string TestConnectionString => System.Configuration.ConfigurationManager.ConnectionStrings["Test"].ConnectionString;

        public static string MainConneectionString => System.Configuration.ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
    }
}