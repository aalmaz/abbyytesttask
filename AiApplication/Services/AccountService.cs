﻿using System.Diagnostics.Contracts;
using System.Linq;
using AiApplication.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace AiApplication.Services
{
    using System.Collections.Generic;

    public class AccountService
    {
        private readonly ApplicationSignInManager signInManager;
        private readonly ApplicationUserManager userManager;

        public AccountService(ApplicationSignInManager signInManager, ApplicationUserManager userManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        public IdentityResult LogIn(ExternalLoginInfo loginInfo)
        {
            Contract.Requires(loginInfo != null);
            Contract.Ensures(Contract.Result<IdentityResult>() != null);

            var user = userManager.FindByEmail(loginInfo.Email);

            if (user != null)
            {
                if (!user.Logins.Any(x => x.LoginProvider == loginInfo.Login.LoginProvider && x.ProviderKey == loginInfo.Login.ProviderKey))
                {
                    userManager.AddLogin(user.Id, loginInfo.Login);
                }
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = signInManager.ExternalSignIn(loginInfo, isPersistent: false);

            if (result == SignInStatus.Success)
            {
                return IdentityResult.Success;
            }

            user = new ApplicationUser { UserName = loginInfo.ExternalIdentity.Name, Email = loginInfo.Email };
            var createResult = userManager.Create(user);
            if (createResult.Succeeded)
            {
                createResult = userManager.AddLogin(user.Id, loginInfo.Login);
                if (createResult.Succeeded)
                {
                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    return IdentityResult.Success;
                }
            }

            return IdentityResult.Failed(string.Empty);
        }

        public IList<UserModel> ListUsers()
        {
            Contract.Ensures(Contract.Result<IList<UserModel>>() != null);

            return userManager.GetAll()
                .Select(x => new UserModel
                {
                    Email = x.Email,
                    Name = x.UserName,
                    SocialNetworks = string.Join(", ", x.Logins.Select(y => y.LoginProvider))
                })
                .ToList();
        }
    }
}