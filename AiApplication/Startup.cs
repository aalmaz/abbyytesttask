﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AiApplication.Startup))]
namespace AiApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
